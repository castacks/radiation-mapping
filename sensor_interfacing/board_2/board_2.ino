/*
 * This script publishes the radiation sensor values from two
 * Teviso RD3024s, integrated over given time interval.
 * This goes on Uno Board 1, Lilred.
 * The output can be seen at 57600 baud.
 * Author: Dhruv Ilesh Shah | Carnegie Mellon University
 */

#include <ros.h>
#include <std_msgs/Int32.h>

const byte rad_1 = 2;
const byte rad_2 = 3;
const byte ledPin = 13;
byte state = LOW;
int timer = 0;
int interval = 1000; // interval of integration, milliseconds
volatile int count_1 = 0;
volatile int count_2 = 0;

ros::NodeHandle n;
std_msgs::Int32 hits_1;
std_msgs::Int32 hits_2;
ros::Publisher sensor_val_pub_1("sensor_val_3", &hits_1);
ros::Publisher sensor_val_pub_2("sensor_val_4", &hits_2);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
  while (!Serial) {
    ; // wait for serial port to connect
  }
  pinMode(ledPin, OUTPUT);  
  pinMode(rad_1, INPUT);
  pinMode(rad_2, INPUT);
  n.initNode();
  n.advertise(sensor_val_pub_1);
  n.advertise(sensor_val_pub_2);
  attachInterrupt(digitalPinToInterrupt(rad_1), sensor_1, RISING);
  attachInterrupt(digitalPinToInterrupt(rad_2), sensor_2, RISING);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin, state);
   Serial.print(timer);
   Serial.print(" ");
   Serial.println(hits_1.data);
   Serial.println(hits_2.data);
  sensor_val_pub_1.publish(&hits_1);
  sensor_val_pub_2.publish(&hits_2);
  n.spinOnce(); // Not really required, as no subscribers in node  
  // Readings integrated over one second
  count_1 = 0;
  count_2 = 0;
  hits_1.data = 0;
  hits_2.data = 0;
  delay(interval);
  state = !state;
  timer++;
}

void sensor_1(){
  count_1++;
  hits_1.data = count_1;
}

void sensor_2(){
  count_2++;
  hits_2.data = count_2;
}
