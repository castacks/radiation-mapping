import csv
import sys
import matplotlib.pyplot as plt
import numpy as np

filename = sys.argv[1]

data = []
data_cps = []

with open(filename, 'rb') as csvfile:
    csv_data = csv.reader(csvfile, delimiter=" ")
    for row in csv_data:
        data.append([int(row[0]), int(row[2])])

# Generating CPS measurements
for i in range(len(data) - 1):
    data_cps.append(data[i + 1][1] - data[i][1])

# Histogram for t-second blocks
t = 2 # Change this parameter
data_cpt = np.reshape(data_cps[:(len(data_cps) / t) * t], (-1, t))
data_cpt = np.sum(data_cpt, axis=1)
print np.shape(data_cpt)

plt.hist(data_cpt, bins=18)
# plt.title("Histogram of counts per " + str(t) + " seconds.")
plt.xlabel("Hits Registered")
plt.ylabel("Frequency")
plt.show()
