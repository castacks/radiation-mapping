/*
 * A simple script to test the Teviso RD2014 Radiation Sensor.
 * The output can be seen on the Serial at 9600 baud.
 */

#include <ros.h>
#include <std_msgs/Int32.h>

const byte radPin = 2;
const byte ledPin = 13;
byte state = LOW;
int timer = 0;
volatile int count = 0;
int oldCount;

ros::NodeHandle n;
std_msgs::Int32 hits;
ros::Publisher sensor_val_pub("sensor_val", &hits);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
  hits.data = 0;
  while (!Serial) {
    ; // wait for serial port to connect
  }
  pinMode(ledPin, OUTPUT);  
  pinMode(radPin, INPUT);
  n.initNode();
  n.advertise(sensor_val_pub);
  attachInterrupt(digitalPinToInterrupt(radPin),blink, CHANGE);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin, state);
  Serial.print(timer);
  Serial.print(" ");
  Serial.println(count);
  Serial.println(hits.data);
//  sensor_val_pub.publish(&hits);
  n.spinOnce();
  count = 0;
  hits.data = 0;
  delay(1000);
  timer++;
}

void blink(){
  ++count;
  hits.data = count;
}

