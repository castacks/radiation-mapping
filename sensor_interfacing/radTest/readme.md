## Activating the sensor node

To activate the sensor node
* Burn the file `radTest.ino` onto your Uno or similar Board
* Launch roscore and run ```rosrun rosserial_python serial_node.py /dev/ttyACM0```

This publishes the topic `/sensor_val` of type `std_msgs/Int32`, which can be called by `sim.cpp`.
