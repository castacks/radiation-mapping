/*
 * This script publishes the radiation sensor values from four
 * Teviso RD3024s, integrated over given time interval.
 * This goes on the Master Board, Lilred. (MEGA 2560)
 * The output can be seen at 57600 baud.
 * Author: Dhruv Ilesh Shah | Carnegie Mellon University
 */

#include <ros.h>
#include <std_msgs/Int32.h>

// Choosing any 4 out of 2, 3, 18-21
const byte rad_1 = 3;
const byte rad_2 = 18;
const byte rad_3 = 20;
const byte rad_4 = 21;

const byte ledPin = 13;
byte state = LOW; // The LED is an indication that board is functional
int timer = 0;
int interval = 1000; // interval of integration, milliseconds
volatile int count_1 = 0;
volatile int count_2 = 0;
volatile int count_3 = 0;
volatile int count_4 = 0;

ros::NodeHandle n;
std_msgs::Int32 hits_1;
std_msgs::Int32 hits_2;
std_msgs::Int32 hits_3;
std_msgs::Int32 hits_4;

ros::Publisher sensor_val_pub_1("sensor_val_1", &hits_1);
ros::Publisher sensor_val_pub_2("sensor_val_2", &hits_2);
ros::Publisher sensor_val_pub_3("sensor_val_3", &hits_3);
ros::Publisher sensor_val_pub_4("sensor_val_4", &hits_4);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
  
  pinMode(ledPin, OUTPUT);  
  pinMode(rad_1, INPUT);
  pinMode(rad_2, INPUT);
  pinMode(rad_3, INPUT);
  pinMode(rad_4, INPUT);
  n.initNode();
  n.advertise(sensor_val_pub_1);
  n.advertise(sensor_val_pub_2);
  n.advertise(sensor_val_pub_3);
  n.advertise(sensor_val_pub_4);
  attachInterrupt(digitalPinToInterrupt(rad_1), sensor_1, RISING);
  attachInterrupt(digitalPinToInterrupt(rad_2), sensor_2, RISING);
  attachInterrupt(digitalPinToInterrupt(rad_3), sensor_3, RISING);
  attachInterrupt(digitalPinToInterrupt(rad_4), sensor_4, RISING);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin, state);
  Serial.print(timer);
  Serial.println(":");
  Serial.println(hits_1.data);
  Serial.println(hits_2.data);
  Serial.println(hits_3.data);
  Serial.println(hits_4.data);
  sensor_val_pub_1.publish(&hits_1);
  sensor_val_pub_2.publish(&hits_2);
  sensor_val_pub_3.publish(&hits_3);
  sensor_val_pub_4.publish(&hits_4);
  n.spinOnce(); // Not really required, as no subscribers in node  
  // Resetting the published data
  count_1 = 0;
  count_2 = 0;
  count_3 = 0;
  count_4 = 0;
  hits_1.data = 0;
  hits_2.data = 0;
  hits_3.data = 0;
  hits_4.data = 0;

  // Readings integrated over interval
  delay(interval);
  state = !state;
  timer++;
}

void sensor_1(){
  count_1++;
  hits_1.data = count_1;
}

void sensor_2(){
  count_2++;
  hits_2.data = count_2;
}

void sensor_3(){
  count_3++;
  hits_3.data = count_3;
}

void sensor_4(){
  count_4++;
  hits_4.data = count_4;
}
