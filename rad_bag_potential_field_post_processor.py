# This script performs post processing source estimation given radiation readings.
# Using the RD3024, this script takes readings from the sensor and selects a number of random locations near the readings.
# The random locations are annotated with an approximate reading, adjusting for the sensor's polar pattern and the distance to the sensor. 
# Frank Mascarich - 4.19.17
# University of Nevada, Reno

import os.path
import sys
import rospy
import rosbag
import numpy as np
from tf import transformations
from visualization_msgs.msg import Marker
import geometry_msgs.msg


def get_translation_rotation(msg):
	trans = msg.transform.translation
	rot = msg.transform.rotation
	return (trans.x, trans.y, trans.z), (rot.x, rot.y, rot.z, rot.w)

def quaternion_multiply(quaternion1, quaternion0):
    x0, y0, z0, w0 = quaternion0
    x1, y1, z1, w1 = quaternion1
    return np.array([-x1 * x0 - y1 * y0 - z1 * z0 + w1 * w0,
                     x1 * w0 + y1 * z0 - z1 * y0 + w1 * x0,
                     -x1 * z0 + y1 * w0 + z1 * x0 + w1 * y0,
                     x1 * y0 - y1 * x0 + z1 * w0 + w1 * z0], dtype=np.float64)

def add_translation(tuple_first, tuple_second):
	return (tuple_first[0]+tuple_second[0], 
		tuple_first[1]+tuple_second[1], 
		tuple_first[2]+tuple_second[2])

def build_marker_message(seq, power, pos, orientation, time_stamp, frame_id="vicon/ZedDrone_/ZedDrone_"):
	marker_msg = Marker()
	marker_msg.header.frame_id = frame_id
	marker_msg.header.stamp = time_stamp
	marker_msg.ns = "raw_rad_data"
	marker_msg.id = seq
	marker_msg.type = 0
	marker_msg.action = 0
	marker_msg.pose.position.x = pos[0]
	marker_msg.pose.position.y = pos[1]
	marker_msg.pose.position.z = pos[2]
	marker_msg.pose.orientation.x = orientation[0]
	marker_msg.pose.orientation.y = orientation[1]
	marker_msg.pose.orientation.z = orientation[2]
	marker_msg.pose.orientation.w = orientation[3]

	marker_msg.scale.x = .33
	marker_msg.scale.y = .05
	marker_msg.scale.z = .05

	if power > 1.0:
		power = 1.0

	marker_msg.color.r = power;
	marker_msg.color.g = 1.0-power;
	marker_msg.color.b = 0;
	marker_msg.color.a = .5;
	marker_msg.lifetime.secs = 0;
	marker_msg.lifetime.nsecs = 1000000;
	return marker_msg

def build_sample_point_message(seq, power, pos, time_stamp):
	marker_msg = Marker()
	marker_msg.header.seq = seq
	marker_msg.header.frame_id = "world"
	marker_msg.header.stamp = time_stamp
	marker_msg.ns = "rad_data_sample_point"
	marker_msg.id = seq
	marker_msg.type = 1
	marker_msg.action = 0
	marker_msg.pose.position.x = pos[0]
	marker_msg.pose.position.y = pos[1]
	marker_msg.pose.position.z = pos[2]

	marker_msg.pose.orientation.x = 0
	marker_msg.pose.orientation.y = 0
	marker_msg.pose.orientation.z = 0
	marker_msg.pose.orientation.w = 1

	marker_msg.scale.x = .1
	marker_msg.scale.y = .1
	marker_msg.scale.z = .1

	if power > 1.0:
		power = 1.0
	marker_msg.color.r = power;
	marker_msg.color.g = 1.0-power;
	marker_msg.color.b = 0;
	marker_msg.color.a = .5;
	marker_msg.lifetime.secs = 2000000;
	marker_msg.lifetime.nsecs = 1000000;
	return marker_msg


def get_transformation(rotation, translation):
	result = transformations.quaternion_matrix(rotation)
	result[0][3] = translation[0]
	result[1][3] = translation[1]
	result[2][3] = translation[2]
	return result

def get_rotation(transformation):
	return transformations.quaternion_from_matrix(transformation)

def get_translation(transformation):
	return (transformation[0][3], transformation[1][3], transformation[2][3])

def get_angle_scale_factor(angle):
	pi = np.pi
	pi_2 = 2.0*pi
	if angle > pi_2:
		angle -= pi_2
	elif angle > pi:
		angle -= pi

	if angle < (pi/3):
		return 1
	elif angle < (2*pi)/3:
		return 1.0/0.75
	else: 
		return 1.0/0.5

if len(sys.argv) < 2:
	print "Must pass input bag file"
	sys.exit(0)

# get the input bag file
input_bag_file = sys.argv[1]
# get the rad box size
#sensor_orientation = int(sys.argv[2])
sensor_orientation = 1
# build the output bag file
output_bag_file = input_bag_file
# if the output bag ends with ".bag", remove it
if output_bag_file.endswith(".bag"):
	output_bag_file = output_bag_file[:-4]
# add a string to the output bag file 
output_bag_file += "_annotated_rad_data.bag"

current_marker = None
markers = []
marker_seq_counter = 0

rad_sensor_right_translation_const = [0,-0.225,0]
rad_sensor_left_translation_const =  [0, 0.225,0]

# looking forward
if sensor_orientation == 0:
	rad_sensor_right_rotation_const = [-0.707,0,0,0.707]
	rad_sensor_left_rotation_const =  [ 0.707,0,0,0.707]

# looking to the sides
elif sensor_orientation == 1:
	rad_sensor_right_rotation_const = [0,0,-0.707,0.707]
	rad_sensor_left_rotation_const =  [0,0, 0.707,0.707]

rad_sensor_left_tranform = get_transformation(rad_sensor_left_rotation_const, rad_sensor_left_translation_const)
rad_sensor_right_tranform = get_transformation(rad_sensor_right_rotation_const, rad_sensor_right_translation_const)


right_translation = None
right_rotation = None

left_rotation = None
left_translation = None

class rad_reading:
	def __init__(self, rotation, translation, sensor_name, reading):
		self.rotation = rotation
		self.translation = translation
		self.transform = get_transformation(rotation, translation)
		self.name = sensor_name
		self.reading = reading
		self.avg_reading = None

	def get_angle_to(self, point):
		t = self.translation
		p = point
		diff = (t[0]-p[0], t[1]-p[1], t[2]-p[2])
		theta = np.arctan(diff[1]/diff[0])
		phi = np.arctan(diff[2]/diff[0])
		return theta

	def get_distance_to(self, point):
		t = self.translation
		p = point
		diff = (t[0]-p[0], t[1]-p[1], t[2]-p[2])
		dist_sq = pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2)
		return np.sqrt(dist_sq)

	def get_adjusted_reading(self, point):
		angle = self.get_angle_to(point)
		dist = self.get_distance_to(point)
		distance_scale_factor = float(1.0/pow(dist,2))
		angle_scale_factor = get_angle_scale_factor(angle)
		temp_reading = self.reading * angle_scale_factor * distance_scale_factor
		return temp_reading


rad_readings = []
first_time_stamp = None
# open the bag
outbag = rosbag.Bag(output_bag_file, 'w')

print "Opened Bag"
in_bag = rosbag.Bag(input_bag_file)
num_messages = in_bag.get_message_count()
message_counter = 0
surface_points = set([])
# loop through the messages
for topic, msg, time_stamp in in_bag.read_messages():
	#if message_counter > 45000:
	#	break
	# print status message
	if first_time_stamp is None:
		first_time_stamp = time_stamp
		#first_time_stamp.secs += 10
	#string_message = "Message " + str(message_counter) + " out of " + str(num_messages) + " messages"
	#sys.stdout.write("\r" + string_message)
	#sys.stdout.flush()
	message_counter += 1
	if "ZedDrone" in topic:
		translation, rotation = get_translation_rotation(msg)
		drone_transformation = get_transformation(rotation, translation)
		# get the transformation to sensor 1
		rad_sensor_left_global = np.dot(drone_transformation, rad_sensor_left_tranform)
		# get the rotation component
		left_rotation = get_rotation(rad_sensor_left_global)
		# get the translation component
		left_translation = get_translation(rad_sensor_left_global)
		# build the message
		left_marker_msg = build_marker_message(marker_seq_counter, 1.0, left_translation, 
			left_rotation, time_stamp, "world")
		# increment the sequence counter to distinguish markers
		marker_seq_counter += 1

		# get the transformation to sensor 2
		rad_sensor_right_global = np.dot(drone_transformation, rad_sensor_right_tranform)
		# get the rotation component
		right_rotation = get_rotation(rad_sensor_right_global)
		# get the translation component
		right_translation = get_translation(rad_sensor_right_global)
		# build the message
		right_marker_msg = build_marker_message(marker_seq_counter, 1.0, right_translation, 
			right_rotation, time_stamp, "world")
		# increment the sequence counter to distinguish markers
		marker_seq_counter += 1

		# write the marker messages to the bag
		outbag.write("/rad_pos_markers", left_marker_msg, time_stamp)
		outbag.write("/rad_pos_markers", right_marker_msg, time_stamp)
	elif left_rotation is not None:
		# if its a rad data message from sensor 1
		if "/M3U/rad_data_1" in topic:
			rad_reading_obj = rad_reading(left_rotation, left_translation, "left", msg.seq)
			rad_readings.append(rad_reading_obj)
		# if its a rad data message from sensor 2
		if "/M3U/rad_data_2" in topic:
			rad_reading_obj = rad_reading(right_rotation, right_translation, "right", msg.seq)
			rad_readings.append(rad_reading_obj)
	if "nbvplanner" in topic:
		if len(msg.markers) > 0:
			for marker in msg.markers:
				for point in marker.points:
					if (point.x, point.y, point.z) not in surface_points:
						print "New point : ", point
						temp_point = (point.x, point.y, point.z)
						surface_points.add(temp_point)
	# whatever the message is, write it to the new output bag
	outbag.write(topic, msg, time_stamp)

print "Found ", len(surface_points), " points"

print "Found ", len(rad_readings), " radiation readings"

sys.exit(0)


num_points = 50000
# get random points
import random

class sample_location:
	def __init__(self, translation):
		self.translation = translation
		self.adjusted_readings = []

	def add_reading(self, translation, adj_reading):
		self.adjusted_readings.append((translation, adj_reading))
		
	def get_distance_to(self, point):
		t = self.translation
		p = point
		diff = (t[0]-p[0], t[1]-p[1], t[2]-p[2])
		dist_sq = pow(diff[0],2) + pow(diff[1],2) + pow(diff[2],2)
		return np.sqrt(dist_sq)

print "Creating Random Points"
random_points = []
x_low  = -400
x_high = 400
y_low  = -400
y_high = 400
z_low  = 0
z_high = 250
for i in range(num_points):
	x = float(random.randrange(x_low, x_high,1))/100.0
	y = float(random.randrange(y_low, y_high,1))/100.0
	z = float(random.randrange(z_low, z_high,1))/100.0
	loc = sample_location((x,y,z))
	random_points.append(loc)

print "Getting \'Adjusted\' Readings"

# loop through the readings in the rad readings list
for reading in rad_readings:
	# loop through the points in the random points
	for point in random_points:
		# adjust the reading for the angle and distance
		adj_reading = reading.get_adjusted_reading(point.translation)
		# add the adjusted reading to the point
		point.add_reading(point.translation, adj_reading)

seq = 0

print "Getting Averages"
# loop through the random points
for point in random_points:
	avg_reading = 0
	for reading in point.adjusted_readings:
		avg_reading += reading[1]
	avg_reading /= len(point.adjusted_readings)
	point.avg_reading = avg_reading
	msg = build_sample_point_message(seq, avg_reading, point.translation, first_time_stamp)
	seq += 1
	outbag.write("/rad_sample_point_markers", msg, first_time_stamp)

import pickle


with open("point_data.txt", "wb") as fp:
	pickle.dump(random_points, fp)




outbag.close()