#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
This file generates the polar calibration plot for the
Teviso RD3024 Sensor, on the calibration data provided by
the Autonomous Robots Lab, University of Nevada, Reno.


Copyright © 2017 by Dhruv Ilesh Shah
shahdhruv@cmu.edu | dhruv.shah@iitb.ac.in
Robotics Institute, Carnegie Mellon University
Indian Institute of Technology, Bombay
"""

import numpy as np
import matplotlib.pyplot as plt
import csv

calib_data_loc = "data/rad_cal_data_first_horizontal_high_power.txt"

data = []
with open(calib_data_loc, 'rb') as csvfile:
    calib_reader = csv.reader(csvfile, delimiter=',')
    for row in calib_reader:
        data.append(row)

data = np.asarray(data[1:], dtype=np.float)
data = np.reshape(data, [12, 24, 3])


# Polar Plot
i = 4
data_i = data[i,:,:]
theta = data_i[:, 1] / 180. * np.pi
mag = data_i[:, 2]

ax = plt.subplot(111, projection='polar')
ax.plot(theta, mag)

ax.grid(True)
plt.show()

# Dependence on Distance
# data_0 = data[:,0,:]

# plt.plot(data_0[:,0], data_0[:,2])
# plt.show()