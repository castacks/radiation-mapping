#ifndef POINT32
	#define POINT32
	#include <geometry_msgs/Point32.h>
#endif

#ifndef CMATH
	#define CMATH
	#include <math.h>
#endif

#ifndef POINT32
	#define POINT32
	#include <geometry_msgs/Point32.h>
#endif

float get_distance(geometry_msgs::Point32 point_a, geometry_msgs::Point32 point_b) {
	float x_dif = point_a.x - point_b.x;
	float y_dif = point_a.y - point_b.y;
	float z_dif = point_a.z - point_b.z;

	float d_sq = pow(x_dif, 2.0) + pow(y_dif, 2.0) + pow(z_dif, 2.0);
	return sqrt(d_sq);
}