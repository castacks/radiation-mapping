#ifndef POINT32
	#define POINT32
	#include <geometry_msgs/Point32.h>
#endif

class sensor {
private:
	geometry_msgs::Point32 _position;
	// A measure of sensitivity with 6D pose?
public:
	// Accessor and Mutator functions
	sensor() {
		_position.x = 1.0;
		_position.y = 0.5;
		_position.z = 0.5;
	}
	void get_position(geometry_msgs::Point32 &position);
	void set_position(geometry_msgs::Point32 position);
};

void sensor::get_position(geometry_msgs::Point32 &position) {
	position = _position;
}

void sensor::set_position(geometry_msgs::Point32 position) {
	_position = position;
}