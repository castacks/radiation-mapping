#ifndef POINT32
	#define POINT32
	#include <geometry_msgs/Point32.h>
#endif

class point_source {
private:
	// Relevant information about the source
	geometry_msgs::Point32 _position;
	float _strength;
public:
	// Accessor and Mutator functions
	point_source() {
		_position.x = 0.0;
		_position.y = 0.0;
		_position.z = 0.0;
	}
	
	void get_position(geometry_msgs::Point32 &position);
	void set_position(geometry_msgs::Point32 position);
	void get_strength(float &strength);
	void set_strength(float strength);
};

void point_source::get_position(geometry_msgs::Point32 &position) {
	position = _position;
}

void point_source::set_position(geometry_msgs::Point32 position) {
	_position = position;
}

void point_source::get_strength(float &strength) {
	strength = _strength;
}

void point_source::set_strength(float strength) {
	_strength = strength;
}