/*
 * This is the sensor pose publisher. As of now, publishes
 * 3D position of the sensor. Can be extended to 6D pose for
 * orientation count.

 * Copyright © 2017 by Dhruv Ilesh Shah
 * shahdhruv@cmu.edu | dhruv.shah@iitb.ac.in
 * Robotics Institute, Carnegie Mellon University
 * Indian Institute of Technology, Bombay
*/

// #include "point_source_sim/sensor.h"

#ifndef ROS_BASE
	#define ROS_BASE
	#include "ros/ros.h"
	#include "std_msgs/String.h"
#endif

#ifndef POINT32
	#define POINT32
	#include <geometry_msgs/Point32.h>
#endif

#ifndef SSTREAM
	#define SSTREAM
	#include <sstream>
#endif

#ifndef TWIST
	#define TWIST
	#include <geometry_msgs/Twist.h>
#endif

geometry_msgs::Point32 position;

void key_callback(const geometry_msgs::Twist &vel) {
	// Manipulating the Twist message for the time being
	position.x -= vel.linear.x;
	position.y -= vel.angular.z;
}

int main(int argc, char **argv) {
	
	position.x = 1.0;
	position.y = 2.0;
	position.z = 2.0;

	ros::init(argc, argv, "sensor_1");
	ros::NodeHandle n;

	ros::Subscriber key_sub = n.subscribe("cmd_vel", 100, key_callback);
	ros::Publisher sensor_1_pub = n.advertise<geometry_msgs::Point32>("sensor_1_posn", 1000);
	
	// Running at 10Hz
	ros::Rate loop_rate(10);
	int count = 0;

	while (ros::ok()) {

		sensor_1_pub.publish(position);

		ros::spinOnce();
		loop_rate.sleep();
		++count;
	}

	return 0;
}