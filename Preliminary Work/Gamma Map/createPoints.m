function [ points ] = createPoints( startX, endX, startY, endY, pointsPerMeter, totalIntensity )
% Written by Jack Buffington 2016

% creates a set of points that each have the same intenstity within the 
% rectangular region defined by the start and end points.
% the points won't strictly end at the end points.  They will start at the
% start points and be spaced as defined by pointsPerMeter.
   
   % calculate how many points will be in each direction
   xPoints = int32((endX - startX)*pointsPerMeter);
   yPoints = int32((endY - startY)*pointsPerMeter);
   
   % figure out their intensity
   totalPoints = xPoints * yPoints;
   intensity = double(totalIntensity)/double(totalPoints);
   
   %create the points
   points = zeros(totalPoints, 3);
   points(:,3) = intensity;
   index = 1;
   for x = 1:xPoints
      xPos = startX + double(x)/pointsPerMeter;
      for y = 1:yPoints
         yPos = startY + double(y)/pointsPerMeter;
         points(index,1:2) = [xPos yPos];
         index = index+1;
      end
   end
end

