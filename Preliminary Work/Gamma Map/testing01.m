% testing01.m
% Written by Jack Buffington 2016

% this script builds the map and then calls renderMap.  After that, it generates an image of 
% what was returned.  THIS SCRIPT CAN TAKE A LONG TIME TO RENDER.  YOU
% MIGHT TRY TURNING DOWN outputPixelsPerMeter TO SOMETHING LOWER TO GET A
% REASONABLE RENDER TIME.  

% The way that I am calculating emitter strength is to calculate:
% ((rad/hr * dist^2)/3.28^2) * 4pi
% this is equivalent to rad/hr * distance in meters^2 *4pi
% This is the value to use for the point emitter.  

% If the source is an area source, then
% take that value and divide it evenly between all points.  This will work fine for more distant 
% radioactive objects but may need to be refined for sources that are closer to the observer. 

% For light, which this is equivalent to, net radiated power = intensity * 4 * pi * r^2

% conversely, intensity = net radiated power / (4*pi*r^2)

% create a 2D model of the train cars in the PUREX tunnel

% below is the how you need to format a call to createPoints
%createPoints( startX, endX, startY, endY, pointsPerMeter, totalIntensity )
ppm = 10; % points per meter 
% I used 'Purex tunnel radiation spreadsheet' for the intensities
car8 = createPoints(1.7,10.7,2.05,3.75,ppm,186);
car7 = createPoints(16.7,23.8,2.15,3.65,ppm,52285);
car6 = createPoints(30,38.9,2.15,3.65,ppm,186);
car5 = createPoints(43.4,51.3,2.4,3.4,ppm,35);
% For car 4 I spread things out evenly, which they probably aren't but 
% I don't have enough data to do better.
car4 = createPoints(54.3,65.9,1.65,4.15,ppm,3137); 
car3 = createPoints(69.7,76.4,2.15,3.65,ppm,11619);
car2 = createPoints(81,93.6,2.4,3.4,ppm,1673);
car1 = createPoints(100.3,105.6,1.7,4.1,ppm,400);

points = [car8;car7;car6;car5;car4;car3;car2;car1];

outputPixelsPerMeter = 30;
outputSize = [108.9 5.8 outputPixelsPerMeter]; % 20: 2178 x 116 95.96 seconds to render
                                               % 30 oppm, 10ppm renderTime: 1141 seconds
                                             
 map = renderMap02(points, outputSize);  
 save('map30C.mat','map');
 
 
 
 % scale the map to a good range
 map(map > 50000) = 2000;  % some pixels are really close to sources and get really bright
 map = sqrt(map);
 map = map / max(max(map));
figure();
imshow(map);

% make a color lookup table
baseColors = [255   0   0;
              255 128   0;
              255 255   0;
              128 255   0;
              0   255   0;
              0   255 128;
              0   255 255;
              0   128 255;
              0   0   255;
              128 0   255;
              255 0   255;
              255 0   128;
              255 128 255;
              128 128 128;
              255 255 255;
              255 255   0];
           
baseColors = baseColors ./ 255;
           
theColors = zeros(256,3);
index = 1;
for I = 1:16
   baseColor = baseColors(I,:);
   for J = 1:16
      theColors(index,:) = ((16-J)* baseColor)/16;
      index = index+1;
   end
end

%colormap(theColors);