function [ gammaImage ] = renderMap( points,outputSize )
   % Written by Jack Buffington 2016
   
   % Creates a map of expected rads per hour around a set of point emitters.
   % This code doesn't take into account shadowing caused by material
   % between the emitter and the point on the map.  
   
   % Points is an array in the form [x y strength]
   %     x and y are in meters.
   %     strength is in (rads * meter^2)/hour  (if I am figuring out my units correctly)

   % Output size is [width height scale] 
   %     width and height are in meters
   %     scale is in pixels/meter


   % calculate the size of the matrix to return
   xPixels = int32(outputSize(1) * outputSize(3));
   yPixels = int32(outputSize(2) * outputSize(3));
   
   gammaImage = zeros(yPixels,xPixels);
   
   % split the points array into points and intensities
   intensities = points(:,3);
   points = points(:,1:2);
   
   
   % Now for each pixel, figure out the output value by finding the
   % contribution from each point
   
   for x = 1:xPixels
      for y = 1:yPixels
         % calculate the position of this pixel
         xPos = double(x)/outputSize(3);
         yPos = double(y)/outputSize(3);
         loc = double([xPos yPos]);
         outputValue = 0;
         for z = 1:size(points,1)
            % find the distance between this pixel and the point
            loc2 = points(z,:);
            dif = loc - loc2;
            distance = sqrt(dif(1)^2 + dif(2)^2);
            
            %find the strength of the source at this point. 
            %intensity = net radiated power / (4*pi*r^2)
            if distance == 0
               thisPointsContribution = 0;
            else
               thisPointsContribution = (intensities(z) / (4*pi*(distance^2)));
            end
            outputValue = outputValue + thisPointsContribution;
         end
         gammaImage(y,x) = outputValue;
      end
   end


end

