function gradDir = findGradientDirectionFromPoints02(X,Y,value)
   % returns the gradient direction in the range of 0->1 given three points  
   % This function treats the value as a Z component and then fits a plane 
   % to the three points.   The gradient of the plane is the gradient of the 
   % triangle

   % Create two vectors from the points
   v1 = [X(2)-X(1) Y(2)-Y(1) value(2)-value(1)];
   v2 = [X(3)-X(1) Y(3)-Y(1) value(3)-value(1)];

   % find the plane's normal by taking the cross product of the two vectors.
   normal = [(v1(2)*v2(3) - v1(3)*v2(2))  (v1(3)*v2(1) - v1(1)*v2(3)) (v1(1)*v2(2) - v1(2)*v2(1))];

   % The resulting vector will be the gradient direction when the Z component
   % is truncated
   gradDir = atan2(-normal(2),-normal(1));
   gradDir = gradDir + pi;  % make it be all positive
   gradDir = gradDir / (2*pi);  % get it into the range of 0->1
end






