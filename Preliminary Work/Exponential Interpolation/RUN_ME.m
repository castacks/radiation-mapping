% RUN_ME.m
% Written by Jack Buffington 2017

% This file, as is, isn't structured really well but does what I want.  
% It renders a number of different maps.   
% It loads a map created in my map generation program to use as a ground
% truth.  
% It then creates a set of points that are to be used as 'sample locations'
% It triangulates these points.
% It does a linear interpolation between the points, which does a somewhat
% decent job but really fails when the points have radically different
% values.  
% It does an inverse square interpolation, which does a much nicer job.   
% It finds the error between these interpolations and the ground truth.
% It then does some preliminary tests for the next thing that I want to
% accomplish, which is extrapolation and map refinement.  Method 1 of
% finding the gradient simply takes the gradient of the rendered
% inverse square interpolation and then blurs it because there is a decent
% amount of noise in the result.   I figured that I could do better so I
% found a different way to find the gradient that is much faster to compute
% and only considers the three points at the corners of the polygon.   That
% is method #2.  


clear
load('extrapolationMap01.mat');
%load('testMap03.mat');

steppedGradient = makeLookupTable01(); % banded raninbow 
smoothColors = makeLookupTable02(); % smooth rainbow  -- better for showing the gradients
steppedColors = makeLookupTable03(); % smooth rainbow  with stepped beginning-- better for showing percent error

divisionsX = 10;
divisionsY = 10;
numPoints = divisionsX * divisionsY;

[X, Y, tri] = makePointsAndTriangles(divisionsX,divisionsY,size(map,2),size(map,1));
%[X, Y, tri] = makePointsAndTrianglesRandomly(size(map,2),size(map,1),100);

% didn't get my draw triangles routine working.  Matlab's routine is OK I
% guess. 
%triImage = drawTriangles(X, Y, tri, size(map,2), size(map,1));

% DRAW THE TRIANGLES
figure
title('Triangulation ');
axis([0 size(map,1) 0 size(map,2)]);
axis equal

hold on
xPoints = zeros(1,4);
yPoints = zeros(1,4);

for I = 1:size(tri,1) 
   % don't draw the extra edge triangles
   if tri(I,1) <= numPoints && ... 
         tri(I,2) <= numPoints && ...
         tri(I,3) <= numPoints
      
      xPoints(1) = X(tri(I,1));
      xPoints(2) = X(tri(I,2));
      xPoints(3) = X(tri(I,3));
      xPoints(4) = X(tri(I,1));
      
      % flip the Y direction
      yPoints(1) = size(map,1) - Y(tri(I,1));
      yPoints(2) = size(map,1) - Y(tri(I,2));
      yPoints(3) = size(map,1) - Y(tri(I,3));
      yPoints(4) = size(map,1) - Y(tri(I,1));

      plot(xPoints, yPoints);
   end
end 




% create an array that has the radiation intensity
intensities = zeros(size(X,1)-4,1);
for I = 1:size(intensities,1)
   intensities(I,1) = map(Y(I,1),X(I,1));
end


image = drawExponentialMap(tri,X,Y,intensities,size(map,2), size(map,1),numPoints);
image2 = drawLinearMap(tri,X,Y,intensities,size(map,2),size(map,1),numPoints);


figure
%subplot(3,3,9);
axis([0 size(map,1) 0 size(map,2)]);


gradientImage2 = zeros(size(map));
imshow(gradientImage2);
hold on;
% figure out the gradient direction for each triangle
for I = 1:size(tri,1) 
   if tri(I,1) <= numPoints && ... 
      tri(I,2) <= numPoints && ...
      tri(I,3) <= numPoints
      
      fprintf('Gradient triangle #%d\n',I);
      XX = [X(tri(I,1)) X(tri(I,2)) X(tri(I,3))];
      YY = [Y(tri(I,1)) Y(tri(I,2)) Y(tri(I,3))];
      value = [intensities(tri(I,1)) intensities(tri(I,2)) intensities(tri(I,3))];
      gradDir = findGradientDirectionFromPoints02(XX,YY,value);
         
      gg = floor(gradDir * 255) + 1;
      c = smoothColors(gg,:);
      f = fill(XX,YY,c);
      
      set(f,'EdgeColor','none'),
   end % of if this is a valid triangle
end
%imshow(gradientImage2);
colormap(smoothColors);
title('Gradient method 2');











percentMap = zeros(size(map));
percentMap2 = zeros(size(map));

% make a map showing the error between reality and the interpolation
for X2 = 1:size(map,2)
   for Y2 = 1:size(map,1)
      actual = map(Y2,X2);
      interpolated = image(Y2,X2);
      delta = abs(actual-interpolated);
      percentMap(Y2,X2) = delta/actual;
      
      interpolated = image2(Y2,X2);
      delta = abs(actual-interpolated);
      percentMap2(Y2,X2) = delta/actual;
   end
end


map = sqrt(map);
mapMax = max(max(map));
map = map / mapMax;

figure
%subplot(3,3,4);
imshow(map);
colormap(steppedGradient);
title('Actual');


figure
%subplot(3,3,2);
imshow(percentMap);
colormap(steppedColors);
title('Percent error - exponential');


image = sqrt(image);
image = image / mapMax;
%subplot(3,3,5);
figure
imshow(image);
colormap(steppedGradient);
title('Interpolated - exponential');



image2 = sqrt(image2);
%image2 = image2 / max(max(image2));
image2 = image2 / mapMax;
%subplot(3,3,6);
figure
imshow(image2);
colormap(steppedGradient);
title('Interpolated - Linear');


figure
%subplot(3,3,3);
imshow(percentMap2);
colormap(steppedColors);
title('Percent error - Linear');





gradDir = findGradientDirectionOfImage(map);
%subplot(3,3,7);
figure
imshow(gradDir);
colormap(smoothColors);
title('Original gradient direction');

%gradDir = findGradientDirectionFromPoints(tri,X,Y,intensities);
gradDir = findGradientDirectionOfImage(image);
gradDir = imgaussfilt(gradDir,3);
%subplot(3,3,8);
figure
imshow(gradDir);
colormap(smoothColors);
title('Gradient method 1');









