RUN_ME.m
   Does various tasks related to interpolation.   Takes some first steps towards extrapolation.

RUN_ME_EXTRAPOLATION.m
   Attempts to figure out where to place emitters based on the gradients that it finds in each 
   triangle.   Draws lines in the directions found.  

all other files are support files.   