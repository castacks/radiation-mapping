function [centroidX,centroidY] = findCentroid(X,Y)
    % X & Y are Nx1 or 1xN matrices
      centroidX = mean(X);
      centroidY = mean(Y);
end