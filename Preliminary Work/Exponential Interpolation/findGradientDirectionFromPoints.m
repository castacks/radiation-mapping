function gradDir = findGradientDirectionFromPoints(X,Y,value)
% returns the gradient direction given three points in the range of 0->1 
   % TODO: try inverse squared scaling these values to see if it is
   % more accurate
   % TODO: try using the triangle's incenter to see if it is more
   % accurate


   % find the centroid of the triangle
   % the centroid is simply the average of the coordinates
   centroid(1) = mean(X);
   centroid(2) = mean(Y);


   % find the angles from the centroid to the three points.
   deltaX = X(1) - centroid(1);
   deltaY = Y(1) - centroid(2);
   angle1 = atan2(deltaY,deltaX);

   deltaX = X(2) - centroid(1);
   deltaY = Y(2) - centroid(2);
   angle2 = atan2(deltaY,deltaX);

   deltaX = X(3) - centroid(1);
   deltaY = Y(3) - centroid(2);
   angle3 = atan2(deltaY,deltaX);

   % normalize the intensities

   value = value / max(value);

   % convert these to X and Y positions using angle and value
   X = value(1)*cos(angle1) + value(2)*cos(angle2) + value(3)*cos(angle3);
   Y = value(1)*sin(angle1) + value(2)*sin(angle2) + value(3)*sin(angle3);

   % convert this into polar coordinates to get the final angle
   gradDir = atan2(Y,X);
   gradDir = gradDir + pi; % eliminate negative numbers
   gradDir = gradDir / (2*pi); % normalize it to 0->1
end






