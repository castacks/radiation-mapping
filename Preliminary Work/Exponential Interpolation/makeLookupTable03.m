function theColors = makeLookupTable03()
   % make a color lookup table
   theColors = zeros(256,3);
%    % this is a simple color wheel
%    for I = 1:42
%       theColors(I,:) = [1 I/42 0];
%    end
%    for I = 1:43
%       theColors(I+42,:) = [1 - I/43 1 0];
%    end
%    
%    for I = 1:42
%       theColors(I+85,:) = [0 1 I/42];
%    end
%    for I = 1:43
%       theColors(I+127,:) = [0 1 - I/43 1];
%    end
%    
%    for I = 1:42
%       theColors(I+170,:) = [I/42 0 1];
%    end
%    for I = 1:43
%       theColors(I+212,:) = [1 0 1-I/43];
%    end
 
   % make a rainbow of colors at the beginning to be better able to tell
   % low percentage errors
   theColors(1,:) = [1 0 0];    % red              0%
   theColors(2,:) = [1 0 0];    % red              
   
   theColors(3,:) = [1 .5 0];   % orange
   theColors(4,:) = [1 .5 0];   % orange
   
   theColors(5,:) = [1 1 0];    % yellow
   theColors(6,:) = [1 1 0];    % yellow
   
   theColors(7,:) = [.5 1 0];   % yellow/green
   theColors(8,:) = [.5 1 0];   % yellow/green
   
   theColors(9,:) = [0 1 0];    % green
   theColors(10,:) = [0 1 0];    % green
   
   theColors(11,:) = [0 1 1];    % aquamarine
   theColors(12,:) = [0 1 1];    % aquamarine
   
   theColors(13,:) = [0 0 1];    % blue
   theColors(14,:) = [0 0 1];    % blue
   
   theColors(15,:) = [.5 0 1];   % blue violet
   theColors(16,:) = [.5 0 1];   % blue violet
   
   theColors(17,:) = [1 0 .5];   % red voilet
   theColors(18,:) = [1 0 .5];   % red voilet
   
   theColors(19,:) = [0 0 0];   % black
   theColors(20,:) = [0 0 0];   % black
   
   theColors(21,:) = [.5 .5 .5];% grey
   theColors(22,:) = [.5 .5 .5];% grey
  
   theColors(23,:) = [1 1 1];   % white            
   theColors(24,:) = [1 1 1];   % white            9.375%
   
end