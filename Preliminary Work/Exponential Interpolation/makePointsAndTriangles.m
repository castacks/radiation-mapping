function [X, Y, tri] = makePointsAndTriangles(rows,cols, width, height)
   numPoints = rows*cols;

   jitterSize = 7;

   xSpacing = floor((width - jitterSize)/(cols-1));
   ySpacing = floor((height - jitterSize)/(rows-1)); 


   %yPos = ySpacing/2;
   yPos = 0;

   index = 1;

   X = zeros(numPoints,1);
   Y = zeros(numPoints,1);

   for I = 1:rows
      xPos = 0;
      for J = 1:cols
         X(index,1) = xPos;
         Y(index,1) = yPos;
         xPos = xPos + xSpacing;
         index = index+1;
      end
      yPos = yPos + ySpacing;
   end 

   X = X + randi(jitterSize,size(X,1),1);
   Y = Y + randi(jitterSize,size(Y,1),1); 


   % at this point it can do a triangulation but the issue is that you get 
   % thin sliver triangles at the edges so I'm putting in four extra points
   % to force the edge points to connect to it instead

   %X = [X;-size(map,1); size(map,1)*2;size(map,1)/2;size(map,1)/2];
   %Y = [Y;size(map,2)/2;size(map,2)/2;-size(map,2);size(map,2)*2];
   
   %X = [X;-size(map,2); size(map,2)*2;size(map,2)/2;size(map,2)/2];
   %Y = [Y;size(map,1)/2;size(map,1)/2;-size(map,1);size(map,1)*2];
   
   X = [X; -width; width*2; width/2; width/2];
   Y = [Y; height/2; height/2; -height; height*2];


   tri = delaunay(X,Y);  % This returns the index of the points for each triangle

end
