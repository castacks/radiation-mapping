function buffer = linearInterpolateDiagonalLine(X,Y,values,buffer)
   deltaX = X(2) - X(1);
   deltaY = Y(2) - Y(1);

   deltaX = abs(deltaX);
   deltaY = abs(deltaY);
   
   accumulator = 0;
   if deltaX > deltaY
      % then it moves more horizontally
      [X1,I1] = min(X(1:2));
      [X2,I2] = max(X(1:2));
      currentY = Y(I1,1);
      deltaV = values(I2,1) - values(I1,1);
      vStep = deltaV / deltaX;
      currentV = values(I1,1);
      
      if Y(I1,1) > Y(I2,1)
         shouldAdd = false;
      else
         shouldAdd = true;
      end

      % the way that I am drawing the line is each time I step one step in
      % the major direction I add the minor value to an accumulator.  When
      % that accumulator gets to the major value then it rolls over to
      % zero.  When it rolls over then it steps one pixel in the minor
      % direction
      for X = X1:X2
         [accumulator, rolledOver] = addToAccumulator(deltaY, accumulator, deltaX);
         buffer(currentY,X) = currentV; % matlab indexes things as row,column
         if rolledOver == true
            if shouldAdd == true
               currentY = currentY + 1;
            else
               currentY = currentY - 1;
            end
         end
         currentV = currentV + vStep;
      end
      
      
   else % then it moves more vertically
      [Y1,I1] = min(Y(1:2));
      [Y2,I2] = max(Y(1:2));
      currentX = X(I1,1);
      deltaV = values(I2,1) - values(I1,1);
      vStep = deltaV / deltaY;
      currentV = values(I1,1);
      
      if X(I1,1) > X(I2,1)
         shouldAdd = false;
      else
         shouldAdd = true;
      end
      
      for Y = Y1:Y2
         [accumulator, rolledOver] = addToAccumulator(deltaX, accumulator, deltaY);
         buffer(Y,currentX) = currentV;
         if rolledOver == true
            if shouldAdd == true
               currentX = currentX + 1; % this could be in error in some cases
            else
               currentX = currentX - 1;
            end
         end
         
         currentV = currentV + vStep;
      end
   end % of if deltaX > deltaY
end % of linearInterpolateDiagonalLine