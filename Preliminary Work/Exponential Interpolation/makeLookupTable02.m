function theColors = makeLookupTable02()
   % make a color lookup table
   theColors = zeros(256,3);
   % this is a simple color wheel
   for I = 1:42
      theColors(I,:) = [1 I/42 0];
   end
   for I = 1:43
      theColors(I+42,:) = [1 - I/43 1 0];
   end
   
   for I = 1:42
      theColors(I+85,:) = [0 1 I/42];
   end
   for I = 1:43
      theColors(I+127,:) = [0 1 - I/43 1];
   end
   
   for I = 1:42
      theColors(I+170,:) = [I/42 0 1];
   end
   for I = 1:43
      theColors(I+212,:) = [1 0 1-I/43];
   end
 
end