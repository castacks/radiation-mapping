load('extrapolationMap01.mat');

%map = sqrt(map);
%map = map / max(max(map));
%figure();
%imshow(map);


% make a color lookup table
baseColors = [255   0   0;
              255 128   0;
              255 255   0;
              128 255   0;
              0   255   0;
              0   255 128;
              0   255 255;
              0   128 255;
              0   0   255;
              128 0   255;
              255 0   255;
              255 0   128;
              255 128 255;
              128 128 128;
              255 255 255;
              255 255   0];
           
baseColors = baseColors ./ 255;
           
theColors = zeros(256,3);
index = 1;
for I = 1:16
   baseColor = baseColors(I,:);
   for J = 1:16
      theColors(index,:) = ((16-J)* baseColor)/16;
      index = index+1;
   end
end
% 
colormap(theColors);









% START OF INTERPOLATION CODE
rows = 7;
cols = 7;
numPoints = rows*cols;

jitterSize = 7;

xSpacing = floor((size(map,1)-jitterSize)/(cols-1));
ySpacing = floor((size(map,2)- jitterSize)/(rows-1)); 


%yPos = ySpacing/2;
yPos = 0;

index = 1;

X = zeros(numPoints,1);
Y = zeros(numPoints,1);

for I = 1:rows
   xPos = 0;
   for J = 1:cols
      X(index,1) = xPos;
      Y(index,1) = yPos;
      xPos = xPos + xSpacing;
      index = index+1;
   end
   yPos = yPos + ySpacing;
end 

X = X + randi(jitterSize,size(X,1),1);
Y = Y + randi(jitterSize,size(Y,1),1); 


% at this point it can do a triangulation but the issue is that you get 
% thin sliver triangles at the edges so I'm putting in four extra points
% to force the edge points to connect to it instead

X = [X;-size(map,1); size(map,1)*2;size(map,1)/2;size(map,1)/2];
Y = [Y;size(map,2)/2;size(map,2)/2;-size(map,2);size(map,2)*2];


tri = delaunay(X,Y);  % This returns the index of the points for each triangle



% draw them
%figure
subplot(2,2,1);
title('Triangulation');
axis([0 size(map,1) 0 size(map,2)]);
axis equal

hold on
xPoints = zeros(1,4);
yPoints = zeros(1,4);



for I = 1:size(tri,1) 
   % don't draw the extra edge triangles
   if tri(I,1) <= numPoints && ... 
         tri(I,2) <= numPoints && ...
         tri(I,3) <= numPoints
      
      xPoints(1) = X(tri(I,1));
      xPoints(2) = X(tri(I,2));
      xPoints(3) = X(tri(I,3));
      xPoints(4) = X(tri(I,1));

      yPoints(1) = Y(tri(I,1));
      yPoints(2) = Y(tri(I,2));
      yPoints(3) = Y(tri(I,3));
      yPoints(4) = Y(tri(I,1));

      plot(xPoints, yPoints);
   end
end 

% create an array that has the radiation intensity
intensities = zeros(size(X,1)-4,1);
for I = 1:size(intensities,1)
   intensities(I,1) = map(Y(I,1),X(I,1));
end

image = drawLinearMap(tri,X,Y,intensities,size(map,2),size(map,1),numPoints);
% 
% % now create a new image that will be the final rendering
% image = ones(size(map))* -1;  % -1 to indicate if nothing is there. 
% 
% % fill in the triangles
% for I = 1:size(tri,1) 
%    % don't draw the extra edge triangles
%    if tri(I,1) <= numPoints && ... 
%          tri(I,2) <= numPoints && ...
%          tri(I,3) <= numPoints
%       fprintf('Working on triangle %d.\n',I);
%       xPts = [X(tri(I,1)); X(tri(I,2)); X(tri(I,3))];
%       yPts = [Y(tri(I,1)); Y(tri(I,2)); Y(tri(I,3))];
%       iPts = [intensities(tri(I,1)); intensities(tri(I,2)); intensities(tri(I,3))];
%       image = linearInterpolateTriangle(xPts,yPts,iPts,image);
%    end
% end
% 
% % Give the places that it didn't have data a value within the range that
% % was seen.5
% image(image == -1) = intensities(1);

%image = sqrt(image);
%image = image / max(max(image));
%figure();
%imshow(image);

colormap(theColors);

percentMap = zeros(size(map));

% make a map showing the error between reality and the interpolation
for X = 1:size(map,2)
   for Y = 1:size(map,1)
      actual = map(Y,X);
      interpolated = image(Y,X);
      delta = abs(actual-interpolated);
      percentMap(Y,X) = delta/actual;
   end
end
subplot(2,2,2);

imshow(percentMap);
title('Percent error');


map = sqrt(map);
map = map / max(max(map));
subplot(2,2,3);
imshow(map);
colormap(theColors);
title('Actual');


image = sqrt(image);
image = image / max(max(image));
subplot(2,2,4);
imshow(image);
title('Interpolated');
