function image = drawLine(image, X, Y)
   deltaX = X(2) - X(1);
   deltaY = Y(2) - Y(1);

   deltaX = abs(deltaX);
   deltaY = abs(deltaY);
   
   accumulator = 0;
   if deltaX > deltaY
      % then it moves more horizontally
      [X1,I1] = min(X(1:2));
      [X2,I2] = max(X(1:2));
      currentY = Y(I1,1);
      vStep = 0;
      currentV = 0;
      
      if Y(I1,1) > Y(I2,1)
         shouldAdd = false;
      else
         shouldAdd = true;
      end

      % the way that I am drawing the line is each time I step one step in
      % the major direction I add the minor value to an accumulator.  When
      % that accumulator gets to the major value then it rolls over to
      % zero.  When it rolls over then it steps one pixel in the minor
      % direction.  This is similar to Bresenham's line drawing algorithm
      % but can do it using only unsigned math.   I came up with this
      % algorithm when I needed to do interpolation on an 8-bit processor
      % that wasn't capable of signed math and didn't have a multiply
      % instruction.  This was WAY faster than Bresenham's algorithm.  
      for X = X1:X2
         [accumulator, rolledOver] = addToAccumulator(deltaY, accumulator, deltaX);
         image(currentY,X) = currentV; % matlab indexes things as row,column
         if rolledOver == true
            if shouldAdd == true
               currentY = currentY + 1;
            else
               currentY = currentY - 1;
            end
         end
         currentV = currentV + vStep;
      end
      
      
   else % then it moves more vertically
      [Y1,I1] = min(Y(1:2));
      [Y2,I2] = max(Y(1:2));
      currentX = X(I1,1);
      vStep = 0;
      currentV = 0;
      
      if X(I1,1) > X(I2,1)
         shouldAdd = false;
      else
         shouldAdd = true;
      end
      
      for Y = Y1:Y2
         [accumulator, rolledOver] = addToAccumulator(deltaX, accumulator, deltaY);
         image(Y,currentX) = currentV;
         if rolledOver == true
            if shouldAdd == true
               currentX = currentX + 1; % this could be in error in some cases
            else
               currentX = currentX - 1;
            end
         end
         
         currentV = currentV + vStep;
      end
   end % of if deltaX > deltaY
end % of linearInterpolateDiagonalLine