key = ones(10,100) * -1;

key(:,1) = 0;
key(:,100) = .11;

key = linearlyFillInTheGaps(key);

imshow(key);
colors = makeLookupTable03();
colormap(colors);