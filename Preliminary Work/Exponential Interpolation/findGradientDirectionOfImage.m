function gradDir = findGradientDirectionOfImage(image)
   %image = imgaussfilt(image,3);
   [gradX, gradY] = gradient(image);

   gradDir = atan2(gradY,gradX);

   gradDir = gradDir + pi;
   gradDir = gradDir/(2*pi);
end