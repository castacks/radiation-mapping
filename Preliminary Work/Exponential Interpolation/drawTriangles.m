function image = drawTriangles(X, Y, tri, width, height)
   image = ones(height, width);
   
   xPoints = zeros(1,4);
   yPoints = zeros(1,4);

   numPoints = size(X,1) - 4;
   
   for I = 1:size(tri,1) 
      % don't draw the extra edge triangles
      if tri(I,1) <= numPoints && ... 
            tri(I,2) <= numPoints && ...
            tri(I,3) <= numPoints
         fprintf('drawTriangles: triangle: %d\n',I);
         xPoints(1) = X(tri(I,1));
         xPoints(2) = X(tri(I,2));
         xPoints(3) = X(tri(I,3));
         xPoints(4) = X(tri(I,1));

         yPoints(1) = Y(tri(I,1));
         yPoints(2) = Y(tri(I,2));
         yPoints(3) = Y(tri(I,3));
         yPoints(4) = Y(tri(I,1));
         %yPoints

         image = linearInterpolateDiagonalLine(xPoints(1:2),yPoints(1:2),[0;0],image);
         image = linearInterpolateDiagonalLine(xPoints(2:3),yPoints(2:3),[0;0],image);
         image = linearInterpolateDiagonalLine(xPoints(3:4),yPoints(3:4),[0;0],image);
      end
   end 
end