% RUN_ME)EXTRAPOLATION.m
% Written by Jack Buffington 2017

% This program is a first attempt to figure out where to place emitters in
% order to extrapolate a new map.   This code is drawing lines showing the
% gradient directon for all of the triangles in the image.   

% I didn't take this any further because I was uncertain how to proceed.  I
% was hoping that the lines would converge a little better than they did.
% Overall things are working mostly as I expected but the gradients are
% intersecting in a broad range of places.  

clear
%load('extrapolationMap01.mat');
load('testMap02.mat');
%load('testMap03.mat');

steppedGradient = makeLookupTable01(); % banded raninbow 
smoothColors = makeLookupTable02(); % smooth rainbow  -- better for showing the gradients
steppedColors = makeLookupTable03(); % smooth rainbow  with stepped beginning-- better for showing percent error

divisionsX = 10;
divisionsY = 10;
numPoints = divisionsX * divisionsY;

[X, Y, tri] = makePointsAndTriangles(divisionsX,divisionsY,size(map,2),size(map,1));
%[X, Y, tri] = makePointsAndTrianglesRandomly(size(map,2),size(map,1),100);


%triImage = drawTriangles(X, Y, tri, size(map,2), size(map,1));

% ######################
% DRAW THE TRIANGULATION
% ######################
figure
title('Triangulation ');
axis([0 size(map,1) 0 size(map,2)]);
axis equal

hold on
xPoints = zeros(1,4);
yPoints = zeros(1,4);

for I = 1:size(tri,1) 
   % don't draw the extra edge triangles
   if tri(I,1) <= numPoints && ... 
         tri(I,2) <= numPoints && ...
         tri(I,3) <= numPoints
      
      xPoints(1) = X(tri(I,1));
      xPoints(2) = X(tri(I,2));
      xPoints(3) = X(tri(I,3));
      xPoints(4) = X(tri(I,1));
      
      % flip the Y direction
      yPoints(1) = size(map,1) - Y(tri(I,1));
      yPoints(2) = size(map,1) - Y(tri(I,2));
      yPoints(3) = size(map,1) - Y(tri(I,3));
      yPoints(4) = size(map,1) - Y(tri(I,1));
%       
%       iPoints(1) = map(xPoints(1),yPoints(1));
%       iPoints(2) = map(xPoints(2),yPoints(2));
%       iPoints(3) = map(xPoints(3),yPoints(3));

      plot(xPoints, yPoints);
%       fill3(xPoints(1,1:3),yPoints(1,1:3),iPoints,'r');
   end
end 








% #####################
% DRAW THE GRADIENT MAP (and also the centroids)
% #####################

% create an array that has the radiation intensity
intensities = zeros(size(X,1)-4,1);
for I = 1:size(intensities,1)
   intensities(I,1) = map(Y(I,1),X(I,1));
end

figure
%subplot(3,3,9);
axis([0 size(map,1) 0 size(map,2)]);

gradientImage2 = zeros(size(map));
imshow(gradientImage2); % this is done just to orient the coordinate system properly and 
                        % make it not be stretched.
hold on;
% figure out the gradient direction for each triangle
for I = 1:size(tri,1) 
   if tri(I,1) <= numPoints && ... 
      tri(I,2) <= numPoints && ...
      tri(I,3) <= numPoints
      
      fprintf('Gradient triangle #%d\n',I);
      XX = [X(tri(I,1)) X(tri(I,2)) X(tri(I,3))];
      YY = [Y(tri(I,1)) Y(tri(I,2)) Y(tri(I,3))];
      value = [intensities(tri(I,1)) intensities(tri(I,2)) intensities(tri(I,3))];
      gradDir = findGradientDirectionFromPoints02(XX,YY,value);
      
      
      % draw the gradient   
      gg = floor(gradDir * 255) + 1;
      c = smoothColors(gg,:);
      f = fill(XX,YY,c);
      set(f,'EdgeColor','none');
      
      % draw the centroid
      [cX,cY] = findCentroid(XX,YY);
      rectangle('Position',[cX-1 cY-1 2 2]);
      
      
   end % of if this is a valid triangle
end

% draw rays going from the centroids in the opposit direction of the gradient 
% I need to do this here because triangles can be drawn over top of the
% rays otherwise
for I = 1:size(tri,1) 
   if tri(I,1) <= numPoints && ... 
      tri(I,2) <= numPoints && ...
      tri(I,3) <= numPoints
   
      XX = [X(tri(I,1)) X(tri(I,2)) X(tri(I,3))];
      YY = [Y(tri(I,1)) Y(tri(I,2)) Y(tri(I,3))];
      value = [intensities(tri(I,1)) intensities(tri(I,2)) intensities(tri(I,3))];
      gradDir = findGradientDirectionFromPoints02(XX,YY,value);
      [cX,cY] = findCentroid(XX,YY);
      % draw a ray extending from the centroid in the direction of the
      % gradient
      gradDirRad = gradDir * 2 * pi;
      rX = -300* cos(gradDirRad) + cX;
      rY = -300* sin(gradDirRad) + cY;
      %if mod(I,10) == 0
         line([cX rX],[cY rY]);
      %end
   end % of if this is a valid triangle
end

%imshow(gradientImage2);
colormap(smoothColors);
title('Gradient method 2');















% ###################
% DRAW THE ACTUAL MAP
% ###################

map = sqrt(map);
mapMax = max(max(map));
map = map / mapMax;
figure
%subplot(3,3,4);
imshow(map);
colormap(steppedGradient);
title('Actual');





% % ##################################
% % DRAW THE EXPONENTIAL INTERPOLATION
% % ##################################
% 
% image = drawExponentialMap(tri,X,Y,intensities,size(map,2), size(map,1),numPoints);
% image = sqrt(image);
% image = image / mapMax;
% %subplot(3,3,5);
% figure
% imshow(image);
% colormap(steppedGradient);
% title('Interpolated - exponential');
% 















