% RUN_ME_EXTRAPOLATION_ELLIPSES.m
% Written by Jack Buffington 2017

% This code is not quite complete.  It is achieving more error than I want
% at the moment.  I'm going to try optimizing the emitter strengths, which
% should improve the output some, but I suspect that it won't be enough.  

% This code is finding the curves of equal potential in the rendered map
% and then fitting ellipses to them.   It then creates lines of emitters
% between the foci of the ellipses.   If figures out the emitter strength
% by finding the emitter strenght that would make the curve be the correct
% strength where the minor axis of the ellipse intersects it. It does this
% through a process of manual integration of all of the emitters inside of
% that ellipse.   

% Once the error is reduced, I would like to further refine this by using
% RANSAC with the points along the curve of equal potential so that this
% algorithm could deal with emitters that were odd shapes like T's, L's ,
% squiggles, or anyting else.  When enough pixels in the curve were within
% a certain distance of the computed ellipse, it would create those emitters, 
% eliminate the matching pixels, and keep searching until it didn't find
% any more ellipses.   It would then do the same refinement of emitter
% strength that I previously described.  


% One further refinement could be to automatically figure out what
% threshold to use to find the ellipses in the first place.  I am currently
% picking them manually.   Alternately, the map could be displayed and I
% could just click on places that looked like they made a nice ellipse.  I
% have noticed that I can reduce error by picking an appropriate threshold.
%  



% Known issues:  
%  * can fail if it runs and doesn't find any ellipses.

clear;
clc;


pixelsPerMeter = 30;
emitterDistance = 3; % approximately how many pixels apart the emitters will be.
threshold = .11 ; % it seems like the lower I set this, the better (to a point).

%load('extrapolationMap01.mat'); % single patch at top center
%load('testMap02.mat'); % a patch on the left and right threshold .25
%load('testMap03.mat'); % bar and patch threshold .35
load('testMap04.mat');
%load('1 1 25.mat'); % a single point
bandedGradient = makeLookupTable01();
lowPercentsColorMap = makeLookupTable03();

map(map > 50000) = 50000;


mapv = sqrt(map); % visible map
maxMap = max(max(mapv));
mapv = mapv/maxMap; % normalizes the map to the range 0->1
figure
subplot(2,2,1)
imshow(mapv);
title('original map');
%colormap(bandedGradient);

% CutoffIntensity has been checked and is correct
cutoffIntensity = (threshold * maxMap)^2; % this is the intensity at the edge of the ellipse

maxVal = max(max(map));
minVal = min(min(map));

mapB = mapv;
mapB(mapB > threshold) = 1; %
%mapB(mapB > testValHigh) = 0;
mapB(mapB ~= 1) = 0;

B = edge(mapB,'Sobel',.3); % .3 seems to give me an appropriate line thickness to 
                           % find the ellipse



C = bwlabel(B);  % replaces the regions of contiguous white pixels in the 
                 % image with consecutive integers 
                 
cMax = max(max(C));
D = C * (1/cMax); % make the regions be different shades of grey
%whiteImage = ones(size(map));
subplot(2,2,2)

imshow(mapB);
title('threshold map');

hold on;
%colormap(bandedGradient);

% B now contains just the edge of the boundary in white
% use RANSAC to randomly select 5 of the points then use SVD to solve for
% the least squares solution for an ellipse.  Eliminate all of the pixels
% that are within three of the ellipse and then repeat until no ellipse is
% found that matches up with more than some threshold of pixels


numberOfEllipses = cMax;
points = [];


for I = 1:numberOfEllipses
   Xc = []; % X coordinates for matching pixels
   Yc = []; % Y coordinates for matching pixels
   for X = 1:size(map,2)
      for Y = 1:size(map,1)
         if C(Y,X) == I
            %pixelList = [pixelList; X Y];
            Xc = [Xc X];
            Yc = [Yc Y];
         end % of if this pixel is the proper value
      end % of going through Y
   end % of going through X
   
   
   % my solution...   Commenting this out even though it probably works
   % because I found ready-made code to find the ellipse parameters and
   % draw them...
%    [u,s,v] = svd([Xc.^2  Xc.*Yc  Yc.^2  x  y  ones(size(Xc))]);
%    ellipseParameters = v(:,6);
%    
   
   X = [Xc;Yc];
   if size(X,2) > 5
      %try
      [z, a, b, alpha] = fitellipse(X);
      [x1,y1,x2,y2] = findFoci(z,a,b,alpha);
      % draw things
      hold on
      plotellipse(z,a,b,alpha,'r');
      plot([x1 x2],[y1 y2],'g');  % draw a line between the foci
      
      % create a set of emitters between the foci
      % set the strength of the emitters to be what would be necessary to
      % make the ellipse be the strength that it is based on the shorter of
      % lengths a and b.  
      
      shortestAxis = min([a b]);
      
      focusDistance = sqrt((x1-x2)^2 + (y1-y2)^2); %distance between the foci
      numberOfPoints = floor((focusDistance/emitterDistance)); % this should put the emitters approximately 
                                                 % (emitterDistance) units apart
                                                 
      actualEmitterDistance = focusDistance/numberOfPoints;
      
      if numberOfPoints == 0
         numberOfPoints = 1;
      end
      
      
      % in the formula below, intensity is a value seen at a measurement
      % point
      % emitterPower = intensity * 4 * pi * r^2
      
      % now create the actual emitters in the form [X Y power]
      if numberOfPoints == 1
         emitterPower = cutoffIntensity * 4 * pi * (shortestAxis/pixelsPerMeter)^2; % 30 is pixels per meter
         tempPoints = [(x1+x2)/2 (y1+y2)/2 emitterPower];
      else
         deltaX = x2 - x1;
         deltaY = y2 - y1;
         percents = 0:1/(numberOfPoints-1):1; % creates an array of values
         % figure out how strong the emitters should be.  To do this, I'll
         % consider each of the emitters and sum their effect eg:
         % field strength = (emitterStrength/4pi) * (1/r1^2 + 1/r2^2 + 1/r3^2 ...)
         % rearranging: 
         % emitterStrength = (fieldStrength * 4pi) / (1/r1^2 + 1/r2^2 + 1/r3^2 ...)
         % I'll consider the point at the end of the minor axis to be my
         % point under observation.  This point will provide the y offset
         % for all of my r's.
         
         % basically I'm doing manual integration
         
         % create the sum of all of the 1/r^2 terms
         
         if mod(numberOfPoints,2) == 0 % the number of points is even
            % then the points are evenly balancedon the left and right.
            num = numberOfPoints/2;
            theSum = 0;
            for I = 1:num
               r = (shortestAxis/pixelsPerMeter)^2 + ((emitterDistance/pixelsPerMeter) * (1+(I-1)*2)/2);
               theSum = theSum + 1/r;
            end
            % I tallied only half of the values so I'm just doubling it
            % here.
            theSum = theSum * 2;
         else % the number of points is odd
            num = (numberOfPoints - 1)/2;
            theSum = 0; % for the center point
            for I = 1:num
               r = (shortestAxis/pixelsPerMeter)^2 + ((emitterDistance/pixelsPerMeter) * I)^2;
               theSum = theSum + 1/r;
            end
            theSum = theSum * 2;
            theSum = theSum + 1/(shortestAxis/pixelsPerMeter)^2; % this is the center point
         end % of if they were odd or even
         
         
         emitterPower = ((cutoffIntensity * 4 * pi)/ theSum);
         
         
         
         tempPoints = [(x1+deltaX*percents); (y1+deltaY*percents); ones(size(percents)) * emitterPower]';
      end
      points = [points;tempPoints];

   end % of if the size was greater than 5

   
end % of going through the ellipses

hold on;
for I = 1: size(points,1)
   if points(I,1) > 0 && ...
      points(I,1) < size(map,1) && ...
      points(I,2) > 0 && ...
      points(I,2) < size(map,2)
   
      plot(points(I,1), points(I,2), '+');
   end
end
%plot(points(:,1),points(:,2),'+');


% render the new map now 
outputSize = [10 10 30];
points (:,1:2) = points(:,1:2) / 30;
[ gammaImage ] = renderMap02( points,outputSize );
   % Creates a map of expected Gamma Radiation around a set of point emitters.
   % Points is an array in the form [x y strength]
   %     x and y are in meters.
   %     strength is in ????

   % Output size is [width height scale] 
   %     width and height are in meters
   %     scale is in pixels/meter
   
gm = gammaImage;
gm(gm > 50000) = 50000;


gm = sqrt(gm); % visible map
%maxMap = max(max(gm));  % use the max from the other map so that they have
                         % the same value range.
gm = gm/maxMap; % normalizes the map to the range 0->1 

subplot(2,2,3);
imshow(gm);
title('generated map');




errorImage = abs(gammaImage-map);
errorImage = errorImage ./ map;

errorImage(errorImage > 1) = 1;

subplot(2,2,4);
imshow(errorImage);

colormap(lowPercentsColorMap);
title('error map');

max(max(map))
max(max(gammaImage))