figure

x1 = 4;
x2 = 7;
y1 = 4;
y2 = 7;

 a = 1/2*sqrt((x2-x1)^2+(y2-y1)^2);  %1/2 of the length between the focii
 b = a*sqrt(1-e^2);
 t = linspace(0,2*pi);
 X = a*cos(t);
 Y = b*sin(t);
 w = atan2(y2-y1,x2-x1);
 x = (x1+x2)/2 + X*cos(w) - Y*sin(w);
 y = (y1+y2)/2 + X*sin(w) + Y*cos(w);
 plot(x,y,'y-')
 axis equal