function theColors = makeLookupTable03()
   % make a color lookup table
   theColors = zeros(256,3);
%    % this is a simple color wheel
%    for I = 1:42
%       theColors(I,:) = [1 I/42 0];
%    end
%    for I = 1:43
%       theColors(I+42,:) = [1 - I/43 1 0];
%    end
%    
%    for I = 1:42
%       theColors(I+85,:) = [0 1 I/42];
%    end
%    for I = 1:43
%       theColors(I+127,:) = [0 1 - I/43 1];
%    end
%    
%    for I = 1:42
%       theColors(I+170,:) = [I/42 0 1];
%    end
%    for I = 1:43
%       theColors(I+212,:) = [1 0 1-I/43];
%    end
 
   % make a rainbow of colors at the beginning to be better able to tell
   % low percentage errors
   theColors(1,:) = [1 0 0];    % red              0%
   theColors(2,:) = [1 0 0];    % red              
   
   theColors(3,:) = [1 .5 0];   % orange
   theColors(4,:) = [1 .5 0];   % orange
   
   theColors(5,:) = [1 1 0];    % yellow
   theColors(6,:) = [1 1 0];    % yellow
   
   theColors(7,:) = [.5 1 0];   % yellow/green
   theColors(8,:) = [.5 1 0];   % yellow/green
   
   theColors(9,:) = [0 1 0];    % green
   theColors(10,:) = [0 1 0];    % green
   
   theColors(11,:) = [0 1 1];    % aquamarine
   theColors(12,:) = [0 1 1];    % aquamarine
   
   theColors(13,:) = [0 0 1];    % blue
   theColors(14,:) = [0 0 1];    % blue
   
   theColors(15,:) = [.5 0 1];   % blue violet
   theColors(16,:) = [.5 0 1];   % blue violet
   
   theColors(17,:) = [1 0 .5];   % red voilet
   theColors(18,:) = [1 0 .5];   % red voilet
   
   theColors(19,:) = [0 0 0];   % black
   theColors(20,:) = [0 0 0];   % black
   
   theColors(21,:) = [.5 .5 .5];% grey
   theColors(22,:) = [.5 .5 .5];% grey
  
   theColors(23,:) = [1 1 1];   % white            
   theColors(24,:) = [1 1 1];   % white            9.375%
   
   theColors(25,:) = [1 .25 .25];    % red              0%
   theColors(26,:) = [1 .25 .25];    % red              
   
   theColors(27,:) = [1 .5 .25];   % orange
   theColors(28,:) = [1 .5 .25];   % orange
   
   theColors(29,:) = [1 1 .25];    % yellow
   theColors(30,:) = [1 1 .25];    % yellow
   
   theColors(31,:) = [.5 1 .25];   % yellow/green
   theColors(32,:) = [.5 1 .25];   % yellow/green
   
   theColors(33,:) = [.25 1 .25];    % green
   theColors(34,:) = [0.25 1 .25];    % green
   
   theColors(35,:) = [0.25 1 1];    % aquamarine
   theColors(36,:) = [0.25 1 1];    % aquamarine
   
   theColors(37,:) = [0.25 0.25 1];    % blue
   theColors(38,:) = [0.25 0.25 1];    % blue
   
   theColors(39,:) = [.5 0.25 1];   % blue violet
   theColors(40,:) = [.5 0.25 1];   % blue violet
   
   theColors(41,:) = [1 0.25 .5];   % red voilet
   theColors(42,:) = [1 0.25 .5];   % red voilet
   
   theColors(43,:) = [0.25 0.25 0.25];   % black
   theColors(44,:) = [0.25 0.25 0.25];   % black
   
   theColors(45,:) = [.75 .75 .75];% grey
   theColors(46,:) = [.75 .75 .75];% grey
  
   theColors(47,:) = [1 1 1];   % white            
   theColors(48,:) = [1 1 1];   % white 
   
   
end