function theColors = makeLookupTable01()
   % make a color lookup table
   baseColors = [255   0   0;
                 255 128   0;
                 255 255   0;
                 128 255   0;
                 0   255   0;
                 0   255 128;
                 0   255 255;
                 0   128 255;
                 0   0   255;
                 128 0   255;
                 255 0   255;
                 255 0   128;
                 255 128 255;
                 128 128 128;
                 255 255 255;
                 255 255   0];

   baseColors = baseColors ./ 255;

   theColors = zeros(256,3);
   index = 1;
   for I = 1:16
      baseColor = baseColors(I,:);
      for J = 1:16
         theColors(index,:) = ((16-J)* baseColor)/16;
         index = index+1;
      end
   end
end