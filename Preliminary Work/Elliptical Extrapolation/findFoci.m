function [x1,y1,x2,y2] = findFoci(center,a,b,angle)
   % center is a 2x1 column vector
   % a is the radius in the direction of the angle
   % b is the radius in the direction perpendicuar to the angle
   % angle is in radians

   % for the needs of this function, a should be larger than b.
   % If it can't be then I need to add some code to swap them and change the
   % angle by pi/2]
   
   if a < b
      c = a;
      a = b;
      b = c;
      angle = angle + pi/2;
   end
   

   % the distance from the foci to the center is 
   % sqrt(a^2 - b^2)

   focusDistance = sqrt(a^2 - b^2);
   
   xOffset = focusDistance * cos(angle);
   yOffset = focusDistance * sin(angle);
   
   x1 = center(1) + xOffset;
   x2 = center(1) - xOffset;
   y1 = center(2) + yOffset;
   y2 = center(2) - yOffset;

end