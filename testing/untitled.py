#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Localization of multiple radiation sources using particle
filter and mean-shift techniques

Copyright © 2017 by Dhruv Ilesh Shah
shahdhruv@cmu.edu | dhruv.shah@iitb.ac.in
Robotics Institute, Carnegie Mellon University
Indian Institute of Technology, Bombay
"""

from environment import *

def visualise_grid(show_sources=False):
    """
    Simple Matplotlib-based visualisation for the grid.
    This includes the particles, sources and sensors.
    """
    plt.close()
    # Plotting the particles
    xpos = []
    ypos = []
    weights = []
    for i in range(len(particles)):
        x, y = particles[i].get_position()
        xpos.append(x)
        ypos.append(y)
        weights.append(particles[i]._weight)
    weights = np.array(weights, dtype=np.float64) * particle_population / 2.
    plt.scatter(xpos, ypos, s=weights, color='black',
                marker='.', label='Particle')

    # Similarly plotting the sources and sensors
    if show_sources:
        xpos = []
        ypos = []
        for i in range(len(sources)):
            x, y = sources[i].get_position()
            xpos.append(x)
            ypos.append(y)
        plt.scatter(xpos, ypos, s=100, color='red', marker='^', label='Source')

    xpos = []
    ypos = []
    for i in range(len(sensors)):
        x, y = sensors[i].get_position()
        xpos.append(x)
        ypos.append(y)
    plt.scatter(xpos, ypos, s=(np.array(xpos) + np.asarray(ypos)) * 5., color='magenta',
                marker='D', label='Sensor')

    plt.legend()
    plt.show(block=False)


def spawn_sensors(num, mode="traversal"):
    """
    Spawns well-positioned sensors in the grid. There are two modes.
    traversal would give symmetrically placed sensors, whereas
    random would assume uniform IID placement of sensors.
    """
    [length, width] = num

    if mode == "traversal":
        length -= 1
        width -= 1
        for i in range(length + 1):
            xpos = grid_size * i / length
            for j in range(width + 1):
                ypos = grid_size * j / width
                sensors.append(sensor(xpos, ypos))
    elif mode == "random":
        for _ in range(length * width):
            xpos, ypos = np.random.uniform(low=0., high=grid_size - 1, size=2)
            sensors.append(sensor(xpos, ypos))

    print str(len(sensors)) + " sensors spawned!"


def spawn_sources(num, mode="random", axis="x"):
    """
    Randomly spawns num sensors in the grid if mode is random.
    If mode is wall, creates a wall of closely-spaced num sensors along axis.
    """
    if mode == "random":
        for _ in range(num):
            xpos, ypos = np.random.uniform(
                low=grid_size / 8, high=grid_size * 7 / 8, size=2)
            sources.append(source(4, xpos, ypos))
    elif mode == "wall":
        xpos, ypos = np.random.uniform(
            low=grid_size / 2, high=grid_size * 3 / 4, size=2)
        for i in range(num):
            sources.append(source(7., xpos + i * 0.5, ypos))
    print str(len(sources)) + " sources spawned!"


def normalise_weights():
    """
    Normalises the weights of the particles so that \sum_i w(p_i) = 1
    """
    sum_of_weights = 0.
    for point in particles:
        sum_of_weights += point.update_weight(1.)
    for point in particles:
        _ = point.update_weight(1 / sum_of_weights)


def spawn_particles(population, factor=1.0):
    """
    Particle Initialization Step.
    Generates the particle population drawn IID from a
    uniform distribution. Some knowledge of source magnitude
    is assumed, but that can be compensated.
    """
    for _ in range(population):
        # Each particle assumed to be under 1uCi
        strength = np.random.uniform(low=4., high=9.)
        # strength = 7  # For the time being, only playing on position
        xpos, ypos = np.random.uniform(low=0., high=grid_size - 1, size=2)
        particles.append(particle(strength, xpos, ypos, factor / population))

    print str(len(particles)) + " particles spawned!"


def update_weights(sensor):
    """
    Particle Weighting Step.
    A lot of ambiguity in this part of the implementation, but
    going ahead with single particle per fusion range model. Also
    assumes known and fixed source-particle intensity.
    """
    reading = sensor.read()
    population = len(particles)
    i = 0
    updated_range = []
    range_weights = []
    print "Next Sensor", str(sensor.get_position())
    while i < population:
        if particles[i].get_distance(sensor.get_position()) < fusion_range:
            # print particles[i].get_position()
            # print reading, particles[i].get_distance(sensor.get_position())
            # Update weight according to Poisson
            expected = particles[i].influence(sensor.get_position())
            factor = (poisson.pmf(floor(reading), expected) +
                      poisson.pmf(ceil(reading), expected)) / 2.
            factor_scaling = np.max(poisson.pmf(
                floor(expected), expected), poisson.pmf(ceil(expected), expected))
            factor /= factor_scaling
            # print "Particle " + str(i)
            # print factor, particles[i]._weight
            range_weights.append(particles[i].update_weight(factor))
            # print factor, particles[i]._weight
            updated_range.append(particles[i])
            particles.remove(particles[i])
            i -= 1
            population -= 1
        i += 1
    print "Weights Updated."
    return updated_range, range_weights


def resample_particles(fusion_range, weights):
    """
    Particle Resampling Step.
    The particles are resampled according to their weights in the
    fusion range of the concerned sensor.
    """
    if not len(weights) == 0:
        weights = np.array(weights, dtype=np.float64)
        weights /= np.sum(weights)  # This gives a probability distribution
        sampled_range = np.random.choice(
            fusion_range, len(fusion_range), p=weights)
        for i in range(len(sampled_range)):
            sampled_range[i].add_noise()
            sampled_range[i] = deepcopy(sampled_range[i])
        sampled_range = deepcopy(list(sampled_range))
        particles.extend(sampled_range)
    normalise_weights()
    population = len(particles)
    print "Particles Resampled."


if __name__ == '__main__':
    particle_population = 100
    sensor_count = [6, 6]
    source_count = 1
    grid_size = 10.
    spawn_sources(source_count, mode="random")
    spawn_sensors(sensor_count, mode="traversal")
    spawn_particles(particle_population)

    time_steps = 10

    visualise_grid(show_sources=True)

    np.random.shuffle(sensors)

    for i in range(time_steps):
        j = 0
        for sensor in sensors:
            updated_range, range_weights = update_weights(sensor)
            resample_particles(updated_range, range_weights)
            print len(particles)
            print i, j
            j += 1
        visualise_grid(show_sources=True)

    plt.show()
