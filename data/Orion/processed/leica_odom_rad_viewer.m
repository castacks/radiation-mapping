%% Load Leica total station odometry data with corresponding radiation measurements, and source locations

test_name = 'test1';

% load leica_odom_rad_data, which is a cell matrix whose rows are dwells 
% and columns are pose and radiation data respectly. the {1,3} entry is a
% special entry containing the locations of sources
load(strcat(test_name, '_leica_odom_rad.mat'))

n_dwells = size(leica_odom_rad_data, 1);
source_locations_odom = leica_odom_rad_data{1,3};
n_sources = size(source_locations_odom);

close all

figure(1)
hold on
axis equal
grid on
xlabel('x')
ylabel('y')
zlabel('z')

% plot pose of Polaris-H in the odom frame (initial position of the
% Velodyne laser scanner) for each dwell point
for dwell = 1:n_dwells
    position = leica_odom_rad_data{dwell, 1}(1:3, 4);
    orientation = leica_odom_rad_data{dwell, 1}(1:3, 1:3);
    plotCamera('Location',position,'Orientation',orientation','Opacity',0, 'Size', 0.2, 'AxesVisible', true);
end

% plot location of sources in the odom frame
for source = 1:n_sources
   scatter3(source_locations_odom(source, 1), source_locations_odom(source, 2), source_locations_odom(source, 3)); 
end

hold off
    
