%% Load Lilred odometry data with corresponding radiation measurements

test_name = 'test7';

% load lilred_odom_rad_data, which is a cell matrix whose rows are dwells 
% and columns are time, pose, and radiation data respectly
load(strcat(test_name, '_lilred_odom_rad.mat'))

n_pts = size(lilred_odom_rad_data, 1);

close all

figure(1)
hold on
axis equal
grid on
xlabel('x')
ylabel('y')
zlabel('z')

step_vis = 10;   % suggested values: test1 and test4 - 200, test7 - 10

% plot a select number of poses of Polaris-H in the odom frame (initial 
% position of the Velodyne laser scanner) for each odometry point
for pt = 1:step_vis:n_pts
    position = lilred_odom_rad_data{pt, 2}(:, 4);
    orientation = lilred_odom_rad_data{pt, 2}(1:3, 1:3);
    
    plotCamera('Location',position(1:3)','Orientation',orientation','Opacity',0, 'Size', 0.2, 'AxesVisible',true);
end

hold off
    
