"""
This script collects data from all the bag files in the directory passed as the first
argument when launching. In particular, it collects the rostopics listed in the
concerned list and generates a CSV file containing the same. The readings are triggered
by the trigger topic.

Author: Dhruv Ilesh Shah
shahdhruv@cmu.edu | dhruv.shah@iitb.ac.in
"""

import rosbag
import numpy as np
import sys
import os
import csv
import yaml

rostopics = ["/polaris_h_rad", "/lilred/odom_ekf"]
trigger = 0  # This stores the index of the trigger topic. Here, /polaris_h_rad

print(chr(27) + "[2J")
print "Entering", sys.argv[1]

for filename in os.listdir(sys.argv[1]):
    print "Opening", filename
    bag = rosbag.Bag(os.path.join(sys.argv[1], filename))
    bag_info = yaml.load(bag._get_yaml_info())
    print "Found bag with duration", bag_info['duration'], "seconds"
    print "Parsing", filename

    csv_file = filename + ".csv"
    csvfile = open(csv_file, 'wb')
    writer = csv.writer(csvfile, delimiter=',')
    flag = 0
    row = []

    for msg in bag.read_messages(topics=rostopics):
        if msg[0] == rostopics[trigger]:
            # Trigger found. Get ready to accept other topics
            flag = 1
            row = []
            # print "- - - - - - - - - - - - - - - - - - - - - - - -"
        if flag:
            if msg[0] in rostopics:
                if msg[0] == "/polaris_h_rad":
                    # print "Rad: ", msg[1].multi_array.layout.dim[0].size
                    row.append(msg[1].multi_array.layout.dim[0].size)
                elif msg[0] == "/lilred/odom_ekf":
                    # print "Odom: ", msg[1].pose.pose.position
                    row.extend([msg[1].pose.pose.position.x, msg[
                               1].pose.pose.position.y, msg[1].pose.pose.position.z])
                    flag = 0
                    writer.writerow(row)
    print "Parsed data. Saved as", csv_file
